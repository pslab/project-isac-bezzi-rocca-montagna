package it.isac.commons.model.sensors;

public class SensorType {
	public final static String MOCK = "mock";
	public final static String GPS = "gps";
	public final static String SOURCE = "source";
	public final static String ACCELERATION = "acceleration";
	public final static String PAYLOAD = "payload";
	public static final String INT_VALUE = "intvalue";
	
}
