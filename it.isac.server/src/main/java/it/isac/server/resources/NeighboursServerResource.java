package it.isac.server.resources;

import it.isac.commons.interfaces.IPosition;
import it.isac.commons.interfaces.resources.INeighboursResource;
import it.isac.commons.model.Node;
import it.isac.commons.model.NodeList;
import it.isac.db.DataBase;
import it.isac.db.ISpatialDataBase;
import it.isac.db.search.SearchCriteria;
import it.isac.server.testing.Timer;
import it.isac.server.utils.ServerConfig;
import it.isac.server.utils.UrlAttributes;

import java.util.List;
import java.util.logging.Logger;

import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;

public class NeighboursServerResource extends ServerResource implements INeighboursResource {

	ISpatialDataBase sb;
	SearchCriteria sc;
	String nodeId, netId;
	Logger timeLogger;
	
	@Override
	protected void doInit() throws ResourceException{
		sb = DataBase.getInstance(); //get db instance
		sc = ServerConfig.getSearchCriteria(); //get def search criteria
		nodeId = getAttribute(UrlAttributes.NODE_ID);
		netId = getAttribute(UrlAttributes.NET_ID);
		timeLogger = ServerConfig.getTimeLogger();
	}
	
	public NodeList represent() {
		
		Timer timer = new Timer(timeLogger, "nbr");
		timer.start();
		//Get position of the node
		Node n = sb.getNode(netId, nodeId);
		IPosition pos = n.getState().getPosition();
		List<Node> nbr = sb.getNeighbourhood(netId, pos, sc);
		timer.end();
		return new NodeList(nbr);
	}

}
