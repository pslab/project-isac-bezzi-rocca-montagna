package it.isac.server.testing;

import java.util.logging.Logger;

/**
 * Used for testing
 * 
 * @author Pievis
 */
public class Timer {

	Logger logger;
	long start, end;
	String type;

	public Timer(Logger logger, String type) {
		this.logger = logger;
		this.type = type;
	}

	public void start() {
		start = System.currentTimeMillis();
	}

	public void end() {
		end = System.currentTimeMillis();
		publish();
	}

	void publish() {
		if (logger == null) {
			return;
		}
		String msg = "";
		long duration = end - start;
		msg = String.format("{\"type\":\"%s\",\"start\":%s,\"end\":%s,\"duration\":%s}",
				type, start + "", end + "", duration + "");
		//System.out.println(msg);
		logger.info(msg);
	}

}
