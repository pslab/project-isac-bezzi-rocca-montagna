package it.isac.server.testing;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;

public class LogTimeReader {

	static ObjectMapper mapper;

	public static void main(String[] args) throws IOException {
		if (!(args.length == 2)) {
			printHelp();
			return;
		}
		String filePath = args[0];
		String separator = args[1]; // "INFO: "
		if (filePath != null) {
			if (filePath == "-h") {
				printHelp();
			} else {
				mapper = new ObjectMapper();
				fetchFromFile(filePath, separator);
			}
		} else
			printHelp();
	}

	static void printHelp() {
		// help printer
		print("LogTimeReader: this simple program read the input .log file and print to the standard output MIN, MAX, AVG time for fetch nbr request and for update state request");
		print("Usage:");
		print("java -jar LogTimeReader [FILEPATH] separator");
		print("-h for print this help");
		print("[FILEPATH] to the .log file produced by the server");
		print("separator is the string before the time information e.g INFO: or INFORMAZIONI:, : included");
	}

	static void fetchFromFile(String filePath, String separator) throws IOException {
		Path path = new File(filePath).toPath();
		List<String> strings = Files.readAllLines(path, Charset.defaultCharset());
		ArrayList<TimeLog> nbr = new ArrayList<TimeLog>();
		ArrayList<TimeLog> update = new ArrayList<TimeLog>();
		for (String s : strings) {
			TimeLog tl = mapper.readValue(getJson(s, separator), TimeLog.class);
			if (tl.type.equals("nbr"))
				nbr.add(tl);
			else if (tl.type.equals("update"))
				update.add(tl);
		}
		printForArray("Nbr", nbr);
		printForArray("Update", update);
	}

	static String getJson(String s, String separator) {
		return s.split(separator)[1];
	}

	static void printForArray(String title, ArrayList<TimeLog> list) {
		long max = 0, min = Long.MAX_VALUE, avg = 0;
		print("-----------------");
		print("Values for: " + title);
		for (TimeLog tl : list) {
			avg += tl.duration;
			if (tl.duration > max)
				max = tl.duration;
			if (tl.duration < min)
				min = tl.duration;
		}
		if (list.size() > 0) {
			avg /= list.size();
			print("MAX: " + max);
			print("MIN: " + min);
			print("AVG: " + avg);
			print("Total Number: "+list.size());
		} else
			print("No update was done");
		print("-----------------");
		print("");
	}

	static void print(String s) {
		System.out.println(s);
	}

	public static class TimeLog {
		String type;
		long duration, start, end;

		public TimeLog() {
		}

		public String getType() {
			return type;
		}

		public void setType(String type) {
			this.type = type;
		}

		public long getDuration() {
			return duration;
		}

		public void setDuration(long duration) {
			this.duration = duration;
		}

		public long getStart() {
			return start;
		}

		public void setStart(long start) {
			this.start = start;
		}

		public long getEnd() {
			return end;
		}

		public void setEnd(long end) {
			this.end = end;
		}

	}
}
