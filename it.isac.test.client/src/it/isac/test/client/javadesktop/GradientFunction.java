package it.isac.test.client.javadesktop;

import java.util.Map;

import javax.naming.NotContextException;

import it.isac.client.impl.device.Domain;
import it.isac.client.impl.device.ExecutionContext;
import it.isac.client.impl.device.FieldCalculusFunction;
import it.isac.commons.interfaces.INodeValue;
import it.isac.commons.interfaces.ISensorSnapshot;
import it.isac.commons.model.NodeState;
import it.isac.commons.model.nodevalues.BasicNodeValue;

public class GradientFunction extends FieldCalculusFunction {

	public GradientFunction(INodeValue startingValue) {
		super(startingValue);
	}

	@Override
	public INodeValue compute(INodeValue localCurrentStates,
			Map<String, NodeState> nbrState,
			Map<String, ISensorSnapshot> localSensors) {
		INodeValue nextValue = new BasicNodeValue(localCurrentStates.getKey());
		ISensorSnapshot source = null;
		// find the source sensor
		for (String k : localSensors.keySet()) {
			if (localSensors.get(k) != null) {
				if (localSensors.get(k).getSensorId().equals("source"))
					source = localSensors.get(k);
			} else {
				nextValue.setValue("Error");
			}
		}
		// is source
		ExecutionContext<?> exec = null;
		try {
			exec = getExecutionContext();
		} catch (Exception e) {
			nextValue.setValue("Error");
			e.printStackTrace();
		}
		if (Boolean.parseBoolean(source.getValue())) {
			nextValue.setValue("0"); // distance 0
		} else { // isn't source
			String keyMinNbr = "";
			double minDist = Double.MAX_VALUE;
			// find the node with min distance
			for (String k : nbrState.keySet()) {
				if (k != exec.getNodeId()) {
					NodeState ns = nbrState.get(k);
					double dist = Double.MAX_VALUE;
					for (INodeValue v : ns.getValues()) {
						if (v.getKey().equals("distGrad")) { // find the correct
														// value
							try {
								dist = Double.parseDouble(v.getValue());
							} catch (Exception ex) {
								dist = Double.MAX_VALUE;
							}
//							System.out.println(k + " distGrad: " + v.getValue());
						}
					}
					if (dist < minDist) {
//						System.out.println(exec.getNodeId()
//								+ " found a new min: " + dist);
						minDist = dist;
						keyMinNbr = k;
					}
				}
			}
			if (keyMinNbr.isEmpty())
				nextValue.setValue("inf");
			else {
				if (exec != null)
					nextValue.setValue(Double.toString(minDist + exec.distanceTo(keyMinNbr)));
			}
		}
		return nextValue;
	}
}
