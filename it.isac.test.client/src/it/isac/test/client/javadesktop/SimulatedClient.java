package it.isac.test.client.javadesktop;

import it.isac.client.impl.device.AggregateFacilitator;
import it.isac.client.impl.device.LatLonExecContext;
import it.isac.client.impl.device.XYExecContext;
import it.isac.commons.interfaces.IPosition;
import it.isac.commons.model.LatLonPosition;
import it.isac.commons.model.nodevalues.BasicNodeValue;
import it.isac.commons.model.sensors.SensorCounterMock;
import it.isac.commons.model.sensors.SensorGPS;
import it.isac.commons.model.sensors.SensorSource;
import it.isac.utils.impl.CAImplDesktop;
import it.isac.utils.impl.ComAdapterFactory;

import java.util.Observable;
import java.util.Observer;
import java.util.Random;

public class SimulatedClient {

	final static int DEVICE_NUMBER = 10;
	final static double MAX_DISTANCE = 0.0025;

	public static void main(String[] args) {
		// Set manager implementation
		ComAdapterFactory.setCAIstance(new CAImplDesktop());

		if (args != null && args.length > 1) {
			boolean isSource = false;
			if (args[0].equals("true"))
				isSource = true;
			double lat, lon;
			lat = Double.parseDouble(args[1]);
			lon = Double.parseDouble(args[2]);
			IPosition position = new LatLonPosition(lat, lon);
			if (args[3].contains(".ini"))
				ComAdapterFactory.configComAdapter(args[3]);
			new SimulatedClient(isSource, position);
			System.out.println("Started at position: " + position.toString());
			System.out.println("is source:" + isSource);
		} else {
			if(args==null) {
				return;
			}
			if (args[0].contains(".ini"))
				ComAdapterFactory.configComAdapter(args[0]);
			// piazza del popolo
			Random random = new Random(43);
			LatLonPosition start = new LatLonPosition(44.137199, 12.241922);
//			new SimulatedClient(true, start);
//			for (int i = 0; i < DEVICE_NUMBER - 1; i++) {
//				double newLon, newLat;
//				newLon = start.getLon() + random.nextDouble() * MAX_DISTANCE;
//				newLat = start.getLat() + random.nextDouble() * MAX_DISTANCE;
//				LatLonPosition pos = new LatLonPosition(newLat, newLon);
//				new SimulatedClient(false, pos);
//				System.out.println("Started at position: " + pos.toString());
//			}
			// stazione
			new SimulatedClient(false, new LatLonPosition(44.145049, 12.249255));
			new SimulatedClient(false, new LatLonPosition(44.145324, 12.249676));
			new SimulatedClient(false, new LatLonPosition(44.145214, 12.248975));
			new SimulatedClient(false, new LatLonPosition(44.145351, 12.249064));
			// new SimulatedClient(false, new LatLonPosition(44.137560,
			// 12.241320));
			// new SimulatedClient(false, new LatLonPosition(44.135996,
			// 12.240365));
			// new SimulatedClient(false, new LatLonPosition(44.140027,
			// 12.242564));
			// new SimulatedClient(false, new LatLonPosition(44.139623,
			// 12.243427));
			// new SimulatedClient(false, new LatLonPosition(44.145154,
			// 12.249463));
		}
	}

	public SimulatedClient(boolean isSource, IPosition position) {
		// simple client for sensor testing
		AggregateFacilitator dev = new AggregateFacilitator((long) 2000, new LatLonExecContext());

		// Create sensor mock
		// SensorCounterMock mock = new SensorCounterMock("mock0");
		// mock.startCounting(1000);
		// create gps mock
		SensorGPS gpsMock = new SensorGPS("gpsMock", position);
		// gpsMock.activateGPS(1000);
		SensorSource source = new SensorSource("source");
		if (isSource) {
			source.setOn();
		}
		// add sensors
		// dev.addRealSensor(mock);
		dev.addRealSensor(gpsMock);
		dev.addRealSensor(source);
		// create starting value
		// BasicNodeValue start = new BasicNodeValue("MockCounterField", "1");
		BasicNodeValue gradStart = new BasicNodeValue("distGrad", "inf");
		// create and add mock field
		// FieldFunctionMock mockFun1 = new FieldFunctionMock(start);
		GradientFunction grad = new GradientFunction(gradStart);
		// grad.setExecutionContext(new LatLonExecContext());
		// Observer mockObserver1 = new Observer() {
		//
		// @Override
		// public void update(Observable o, Object arg) {
		// // System.out.println("New value received from 1");
		// // System.out.println((String) arg);
		// }
		// };
		Observer mockObserver2 = new Observer() {

			@Override
			public void update(Observable o, Object arg) {
				// System.out.print("Gradient Value: ");
				// System.out.println((String) arg);
			}
		};
		// dev.addField(mockFun1, mockObserver1);
		dev.addField(grad, mockObserver2);
		dev.start();
	}
}
