package it.isac.test.client.stresstest;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.Callback;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import it.isac.commons.interfaces.resources.INeighboursResource;
import it.isac.commons.model.Node;
import it.isac.commons.model.NodeList;
import it.isac.commons.model.NodeState;
import it.isac.commons.requestresponse.IdClass;
import it.isac.commons.requestresponse.SimpleResponse;
import it.isac.utils.impl.ComAdapterFactory;
import it.isac.utils.impl.ComAdapterImpl;

/* 
 * This implementation use OkHttp for asynchronously contact the server
 * via GET and POST request.
 * Only exception is joinNetwork: since every device require a nodeId
 * to live in the network, this request is sinchronous.
 * Response will be ignored and null value will be pass to the caller.
 * Jackson is used for parsing object to json (and vice versa).
 * For more information on OkHttp:
 * http://square.github.io/okhttp/
 * https://github.com/square/okhttp
 */

public class CMImplAsync extends ComAdapterImpl {

	public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
	OkHttpClient client = new OkHttpClient();

	@Override
	public String joinNetwork(NodeState state) {
		// url
		String url = ComAdapterFactory.BASEURL + ComAdapterFactory.NETID + "/nodes/";
		System.out.println("URL:" + url);
		// conversion with jackson
		ObjectMapper mapper = new ObjectMapper();
		String jsonState = null;
		try {
			// serialize
			jsonState = mapper.writeValueAsString(state);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (jsonState != null) {
			// create the request
			RequestBody body = RequestBody.create(JSON, jsonState);
			Request request = new Request.Builder().url(url).post(body).build();
			try {
				// sync
				Response response = client.newCall(request).execute();
				SimpleResponse sr = mapper.readValue(response.body().string(), SimpleResponse.class);
				if (sr.isSuccess()) // deserialize
					return ((IdClass) sr.getData()).getId();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	@Override
	public NodeList fetchNeighbour(String nodeId) {
		String url = ComAdapterFactory.BASEURL + ComAdapterFactory.NETID + "/nodes/" + nodeId + "/nbr/";
		Request request = new Request.Builder().url(url).build();
		// ASYNC GET
		try {
			// response = client.newCall(request).execute();
			client.newCall(request).enqueue(new Callback() {
				@Override
				public void onResponse(Response response) throws IOException {
					// if (!response.isSuccessful())
					// throw new IOException("Unexpected code " + response);
					// System.out.println("Response received, but who cares?");
					// String jsonResp = response.body().string();
					// System.out.println("Json Nbr:" + jsonResp);
					// ObjectMapper mapper = new ObjectMapper();
					// NodeList res = mapper.readValue(jsonResp,
					// NodeList.class);
				}

				@Override
				public void onFailure(Request request, IOException e) {
					// e.printStackTrace();

				}
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void sendState(String nodeId, NodeState state) {
		// url
		String url = ComAdapterFactory.BASEURL + ComAdapterFactory.NETID + "/nodes/" + nodeId + "/";
		// conversion with jackson
		ObjectMapper mapper = new ObjectMapper();
		String jsonState = null;
		Node node = new Node(nodeId,state);
		try {
			// serialize
			jsonState = mapper.writeValueAsString(node);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (jsonState != null) {
			// create the request
			RequestBody body = RequestBody.create(JSON, jsonState);
			Request request = new Request.Builder().url(url).put(body).build();
			
			client.newCall(request).enqueue(new Callback() {

				@Override
				public void onResponse(Response response) throws IOException {
					//if (!response.isSuccessful())
					//	throw new IOException("Unexpected code " + response);
					//System.out.println("Response received, but who cares?");
					// String jsonResp = response.body().string();
					// System.out.println("Json Nbr:" + jsonResp);
					// ObjectMapper mapper = new ObjectMapper();
					// NodeList res = mapper.readValue(jsonResp,
					// NodeList.class);
				}

				@Override
				public void onFailure(Request request, IOException e) {

				}
			});
		}
	}

}
