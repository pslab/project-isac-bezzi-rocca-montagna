package it.isac.test.client.stresstest;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import it.isac.commons.interfaces.ISensorSnapshot;
import it.isac.commons.model.LatLonPosition;
import it.isac.commons.model.NodeState;
import it.isac.commons.model.XYPosition;
import it.isac.commons.model.sensors.SensorGPS;
import it.isac.utils.impl.ComAdapterFactory;
import it.isac.utils.impl.ComunicationAdapter;

public class SimpleStressClient {

	final static double MAX_DISTANCE = 0.0005;
	long FREQ = 1000;

	public static void main(String[] args) {
		int deviceNum;
		int randomSeed;
		long freq = 1000;
		String iniFile;
		ComAdapterFactory.setCAIstance(new CMImplAsync());
		if (args.length == 4) {
			deviceNum = Integer.parseInt(args[0]);
			randomSeed = Integer.parseInt(args[1]);
			freq = Long.parseLong(args[2]);
			iniFile = args[3];
		} else {
			print("Errore nei parametri inseriti");
			printHelp();
			return;
		}
		ComAdapterFactory.configComAdapter(iniFile);
		// Random
		Random rnd = new Random(randomSeed);
		LatLonPosition start = new LatLonPosition(44.137199, 12.241922);
		new SimpleStressClient(freq, start).StartClient();
		System.out.println("+ Starting clients");
		for (int i = 0; i < deviceNum - 1; i++) {
			double newLon, newLat;
			int sign = rnd.nextInt() % 2 == 0 ? 1 : -1;
			newLon = start.getLon() + sign * rnd.nextDouble() * MAX_DISTANCE;
			newLat = start.getLat() + sign * rnd.nextDouble() * MAX_DISTANCE;
			LatLonPosition pos = new LatLonPosition(newLat, newLon);
			new SimpleStressClient(freq, pos).StartClient();
			//System.out.println("Started at position: " + pos.toString());
		}
		System.out.println("+ All client started");
	}

	private static void printHelp() {
		print("SimpleStressClient: this program is used to stress test the server. It simply spawn multiple client, every client will do an async request of neighbour and a nbr fetch request to the server");
		print("Usage:");
		print("java -jar SimpleStressClient numberOfClient seed freq [CONFIGPATH]");
		print("-h for print this help");
		print("numberOfClient is the number of (parallel) client t be spawned");
		print("seed is the seed for the random. A fixed seed will allow the user to produce a fixed sequence of random numbers");
		print("freq is the frequency, in milliseconds, at which clients will send packets to the server");
		print("[CONFIGPATH] is the path to the configuration file for the ComunicationAdapter");
	}

	private static void print(String s) {
		System.out.println(s);
	}

	ScheduledThreadPoolExecutor exec;
	Thread behaviour;
	ComunicationAdapter cm;
	String nodeId;
	NodeState ns;
	long freq;

	public SimpleStressClient(long frequency, LatLonPosition pos) {
		this.freq = frequency;

		exec = new ScheduledThreadPoolExecutor(1);

		cm = ComAdapterFactory.getCMIstance();
		ns = buildDummyState(pos);
		nodeId = cm.joinNetwork(ns);
		System.out.println("- Client joined");
		if (nodeId != null)
			log("nodeId:" + nodeId);

		behaviour = new Thread(new Runnable() {
			@Override
			public void run() {
				// log("Testing the factory...");
				cm.fetchNeighbour(nodeId);
				// log("nbr fetch request sent");
				cm.sendState(nodeId, ns);
				// log("state sent");
			}
		});
	}

	public void StartClient() {
		exec.scheduleWithFixedDelay(behaviour, freq, freq, TimeUnit.MILLISECONDS);
	}

	public NodeState buildDummyState(LatLonPosition pos) {
		String idGps = "gps1";
		// GPS
		// XYPosition pos = new XYPosition(1, 2);
		SensorGPS gps = new SensorGPS(idGps, pos);
		// dummy state
		NodeState ns = new NodeState();
		ns.setPosition(pos);
		ArrayList<ISensorSnapshot> sensors = new ArrayList<ISensorSnapshot>();
		sensors.add(gps.getValue());
		ns.setSensors(sensors);
		return ns;
	}

	static void log(String s) {
		System.out.println(s);
	}

}
