package it.isac.client.impl.device;

import it.isac.commons.interfaces.IPosition;
import it.isac.commons.model.LatLonPosition;
import it.isac.commons.model.NodeState;
import it.isac.commons.model.PositionType;
import it.isac.commons.model.Unit;

public class LatLonExecContext extends ExecutionContext<LatLonPosition> {
	public Unit unitType = Unit.M;

	@Override
	public double distanceTo(String target) {
		if (domain != null) { // sanity check: domain will be late assigned
			NodeState nbr = domain.getNbr(target);
			LatLonPosition myPos = getDevicePosition();
			IPosition nbrPos = nbr.getPosition();
			if (nbrPos.getPositionType() == PositionType.LATLON)
				return LatLonPosition.distance(myPos, (LatLonPosition) nbrPos,
						unitType);
		}
		return Double.NaN;
	}

	@Override
	public LatLonPosition getDevicePosition() {
		if (domain != null) { // sanity check: domain will be late assigned
			if (domain.getPosition().getPositionType() == PositionType.LATLON)
				return (LatLonPosition) domain.getPosition();
		}
		return null;
	}
}
