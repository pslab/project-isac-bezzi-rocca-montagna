package it.isac.client.impl.device;

import it.isac.commons.interfaces.IPosition;
import it.isac.commons.model.NodeState;
import it.isac.commons.model.PositionType;
import it.isac.commons.model.XYPosition;

public class XYExecContext extends ExecutionContext<XYPosition> {

	@Override
	public double distanceTo(String target) {
		if (domain != null) { // sanity check: domain will be late assigned
			NodeState nbr = domain.getNbr(target);
			XYPosition myPos = getDevicePosition();
			IPosition nbrPos = nbr.getPosition();
			if (nbrPos.getPositionType() == PositionType.XY)
				return XYPosition.distance(myPos, (XYPosition) nbrPos);
		}
		return Double.NaN;
	}

	@Override
	public XYPosition getDevicePosition() {
		if (domain != null) { // sanity check: domain will be late assigned
			if (domain.getPosition().getPositionType() == PositionType.XY)
				return (XYPosition) domain.getPosition();
		}
		return null;
	}

}
