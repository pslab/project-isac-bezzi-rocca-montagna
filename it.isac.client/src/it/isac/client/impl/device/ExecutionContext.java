package it.isac.client.impl.device;

public abstract class ExecutionContext<PosType> {
	Domain domain;
	public abstract double distanceTo(String target);
	public abstract PosType getDevicePosition();
	public void setDomain(Domain domain) {
		this.domain = domain;
	}
	public String getNodeId(){
		if(domain!=null)
			return domain.getNodeId();
		else return "";
	}
}
