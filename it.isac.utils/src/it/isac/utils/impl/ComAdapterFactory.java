package it.isac.utils.impl;

import java.io.BufferedReader;
import java.io.FileReader;
import it.isac.utils.impl.SCMLogger;
import it.isac.utils.interfaces.IComAdapterImpl;

public class ComAdapterFactory {
	// TODO: you can add here some imformation useful to comunicate
	// static final String BASEURL = "http://192.168.43.87:8111";
	public static String BASEURL = "http://localhost:8111";
	public static String NETID = "/net0";
	private static ComunicationAdapter CMimpl;

	public static void setCAIstance(IComAdapterImpl impl) {
		CMimpl = new ComunicationAdapter(impl);
	}

	public static ComunicationAdapter getCMIstance() {
		if (CMimpl == null) {
			try {
				throw new Exception("Comunication Manager Not istantiated");
			} catch (Exception ex) {
			}
		}
		return CMimpl;
	}
	
	
	public static void configComAdapterBaseUrl(String url, String port){
		BASEURL="http://" + url +":" + port;	
	}
	public static void configComAdapterNetId(String netid){
		NETID = "/" + netid;	
	}
	

	public static void configComAdapter(String configFile) {
		// configure the comunication manager by reading the input file
		try (BufferedReader reader = new BufferedReader(new FileReader(
				configFile))) {
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (!line.startsWith("//")) { // comment, skip this line
					String val = line.split("=")[1].trim();// get the value
					if (line.contains("BaseUrl"))
						BASEURL = "http://" + val; // change ip
					if (line.contains("Port"))
						BASEURL += ":" + val; // add port
					if (line.contains("NetId"))
						NETID = "/" + val; // change net
				}
			}
		} catch (Exception x) {
			SCMLogger.error("Error while configurating the comunication manager: %s%n"+x);
		}
		// if an error occurs (or the methods was not invoked), default value is
		// used
		SCMLogger.log("Starting client with the following target server and network:"
						+ BASEURL + NETID);
	}
}
