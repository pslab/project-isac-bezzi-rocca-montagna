package it.isac.utils.impl;

import org.restlet.resource.ClientResource;

import com.fasterxml.jackson.databind.ObjectMapper;

import it.isac.commons.requestresponse.IdClass;
import it.isac.commons.interfaces.resources.INeighboursResource;
import it.isac.commons.interfaces.resources.INodeResource;
import it.isac.commons.interfaces.resources.INodesResource;
import it.isac.commons.model.Node;
import it.isac.commons.model.NodeList;
import it.isac.commons.model.NodeState;
import it.isac.commons.requestresponse.SimpleResponse;
import it.isac.utils.interfaces.ICAImplDesktop;

public class CAImplDesktop extends ComAdapterImpl implements ICAImplDesktop {

	public String joinNetwork(NodeState state) {
		// Join network service
		ClientResource service = new ClientResource(ComAdapterFactory.BASEURL);
		INodesResource nodesRe = service.getChild(ComAdapterFactory.NETID + "/nodes/", INodesResource.class);
		String idNode = "";
		try {
			// addNode without id will create a new node
			SimpleResponse sr = nodesRe.addNode(state); // POST
			// check
			if (sr.isSuccess())
				idNode = ((IdClass) sr.getData()).getId();
			else
				System.err.println("Something wrong during joining.\n" + sr.getMessage());

			//System.out.println("Network joined");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return idNode;
	}

	public NodeList fetchNeighbour(String nodeId) {
		ClientResource service = new ClientResource(ComAdapterFactory.BASEURL);
		INeighboursResource nbrRes = service.getChild(ComAdapterFactory.NETID + "/nodes/" + nodeId + "/nbr/",
				INeighboursResource.class);
		NodeList res = nbrRes.represent(); // GET
		//System.out.println("Nbr fetched");
		return res;
	}

	public void sendState(String nodeId, NodeState state) {
		ClientResource service = new ClientResource(ComAdapterFactory.BASEURL);
		INodeResource nodesRes = service.getChild(ComAdapterFactory.NETID + "/nodes/" + nodeId+"/", INodeResource.class);
		// update node state with id = nodeId
		try {
		Node n = new Node(nodeId, state);
//		ObjectMapper mapper = new ObjectMapper();
//		System.out.println(mapper.writeValueAsString(n));
		SimpleResponse sr = nodesRes.update(n); // POST
		// check
		if (sr != null) {
			if (!sr.isSuccess())
				System.err.println("Error while sending new state: " + sr.getMessage());
		} else
			System.err.println("Something wrong with the server: external server error");
			//System.out.println("State sent");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
