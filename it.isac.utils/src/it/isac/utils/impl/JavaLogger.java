package it.isac.utils.impl;

public class JavaLogger extends SCMLogger{

	@Override
	public void logImpl(String s) {
		System.out.println(s);	
	}

	@Override
	public void errImpl(String s) {
		System.err.println(s);		
	}

}
