package it.isac.utils.impl;

public abstract class SCMLogger {
	protected static SCMLogger logger;

	//Abstract methods to be implemented from acutal logger
	protected abstract void logImpl(String s);
	protected abstract void errImpl(String s);
	
	
	private static void defaultLogger(){
		logger=new SCMLogger(){
			@Override
			protected void logImpl(String s) {
				//nothing logged
			}
			
			@Override
			protected void errImpl(String s) {
				//no error reported
			}
			
		};
	}
	
	public static void setLogger(SCMLogger l){
		logger=l;
	}
	
	public synchronized static void log(String s){
		if(logger==null){
			defaultLogger();
		}
		logger.logImpl(s);
	}
	
	public synchronized static void log(Object o){
		if(logger==null){
			defaultLogger();
		}
		logger.logImpl(o.toString());
	}
	
	public synchronized static void error(String s){
		if(logger==null){
			defaultLogger();
		}
		logger.errImpl(s);
	}
	
	
}
