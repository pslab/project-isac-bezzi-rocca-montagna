package it.isac.droidclient.misc;

public class DeviceParameters {
	public static final String configFilename = "config.ini";
	
	//Network
	public static String ConnectionType="Server";
	public static String ServerIpAddress="192.168.0.101";
	public static String ServerPort="8111";
	public static String NetworkID="net0";
	
	//GPS
	public static Boolean isFixGPS=true;
	public static Boolean isLatLon=true;
	
	//Performances
	public static int RefreshPeriod=2000;
	
	
	//Sensor Values
	public static Boolean BoolSens1=false;
	public static Boolean BoolSens2=false;
	public static int intValueSens=0;
	public static Double fixLatY=44.4194877d;
	public static Double fixLonX=12.1956804d;
	
	public static FunctionType FunType=FunctionType.Gradient;
	
	
	//TODO lasciare Enum o altro metodo?
	public enum FunctionType{
		Gradient, Test, Gossip, MsgGradient, PointingGradient
	}
}

