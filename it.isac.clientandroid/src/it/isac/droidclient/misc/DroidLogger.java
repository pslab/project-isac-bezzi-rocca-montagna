package it.isac.droidclient.misc;

import android.util.Log;
import it.isac.utils.impl.SCMLogger;

public class DroidLogger extends SCMLogger {
	String tag="";
	
	public DroidLogger(String s) {
		tag=s;
	}
	
	public DroidLogger() {
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void logImpl(String s) {
		Log.d(tag,s);
	}

	@Override
	protected void errImpl(String s) {
		Log.e(tag,s);
	}

}
