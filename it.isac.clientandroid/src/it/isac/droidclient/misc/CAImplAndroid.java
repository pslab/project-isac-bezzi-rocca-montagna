package it.isac.droidclient.misc;

import org.restlet.resource.ClientResource;

import android.util.Log;
import android.widget.Toast;
import it.isac.commons.interfaces.resources.INeighboursResource;
import it.isac.commons.interfaces.resources.INodeResource;
import it.isac.commons.interfaces.resources.INodesResource;
import it.isac.commons.model.Node;
import it.isac.commons.model.NodeList;
import it.isac.commons.model.NodeState;
import it.isac.commons.requestresponse.IdClass;
import it.isac.commons.requestresponse.SimpleResponse;
import it.isac.utils.impl.ComAdapterFactory;
import it.isac.utils.impl.ComAdapterImpl;
import it.isac.utils.impl.SCMLogger;
import it.isac.utils.interfaces.ICAImplAndroid;


public class CAImplAndroid extends ComAdapterImpl implements ICAImplAndroid {
	


	public String joinNetwork(NodeState state) {
		// Join network service
		SCMLogger.log("Join Network > Connecting to " + ComAdapterFactory.BASEURL);
		ClientResource service = new ClientResource(ComAdapterFactory.BASEURL);
		INodesResource nodesRe = service.getChild(ComAdapterFactory.NETID + "/nodes/", INodesResource.class);
		SCMLogger.log("Connection: " + nodesRe);

		String idNode = "";
		try {
			// addNode without id will create a new node
			SimpleResponse sr = nodesRe.addNode(state); // POST
			// check
			if (sr.isSuccess()){
				idNode = ((IdClass) sr.getData()).getId();
			}else{
				SCMLogger.log("Something wrong during joining.\n" + sr.getMessage());
			}
			SCMLogger.log("NetworkJoined! " + sr.getMessage());
		} catch (Exception e) {
			SCMLogger.log("Connection error");
			e.printStackTrace();
		}
		return idNode;
	}

	public NodeList fetchNeighbour(String nodeId) {
		SCMLogger.log("Fetch Neighbour > Connecting to " + ComAdapterFactory.BASEURL);
		ClientResource service = new ClientResource(ComAdapterFactory.BASEURL);
		INeighboursResource nbrRes = service.getChild(ComAdapterFactory.NETID + "/nodes/" + nodeId + "/nbr/",
				INeighboursResource.class);
		NodeList res = nbrRes.represent(); // GET
		SCMLogger.log("Nbr fetched");
		return res;
	}

	public void sendState(String nodeId, NodeState state) {
		SCMLogger.log("Send State > Connecting to " + ComAdapterFactory.BASEURL);
		ClientResource service = new ClientResource(ComAdapterFactory.BASEURL);
		INodeResource nodesRes = service.getChild(ComAdapterFactory.NETID + "/nodes/" + nodeId+"/", INodeResource.class);
		// update node state with id = nodeId
		try {
		Node n = new Node(nodeId, state);
//		ObjectMapper mapper = new ObjectMapper();
//		Logger.log(mapper.writeValueAsString(n));
		SimpleResponse sr = nodesRes.update(n); // POST
		// check
		if (sr != null) {
			if (!sr.isSuccess()){
				SCMLogger.log("Error while sending new state: " + sr.getMessage());
			}
		} else{
			SCMLogger.log("Something wrong with the server: external server error");
		}
		SCMLogger.log("State sent");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

}
