package it.isac.droidclient.activities;

import java.awt.List;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;

import org.restlet.engine.Engine;
import org.restlet.ext.jackson.JacksonConverter;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import it.isac.client.impl.device.AggregateFacilitator;
import it.isac.client.impl.device.FieldCalculusFunction;
import it.isac.client.impl.device.LatLonExecContext;
import it.isac.commons.interfaces.INodeValue;
import it.isac.commons.interfaces.IPosition;
import it.isac.commons.model.LatLonPosition;
import it.isac.commons.model.Position;
import it.isac.commons.model.XYPosition;
import it.isac.commons.model.nodevalues.BasicNodeValue;
import it.isac.commons.model.nodevalues.TupleValue;
import it.isac.commons.model.sensors.SensorGPS;
import it.isac.commons.model.sensors.SensorSource;
import it.isac.droidclient.R;
import it.isac.droidclient.functions.FieldFunctionMock;
import it.isac.droidclient.functions.GossipFunction;
import it.isac.droidclient.functions.GradMsgFunction;
import it.isac.droidclient.functions.GradPointingFunction;
import it.isac.droidclient.functions.GradientFunction;
import it.isac.droidclient.misc.CAImplAndroid;
import it.isac.droidclient.misc.DeviceParameters;
import it.isac.droidclient.misc.DroidLogger;
import it.isac.droidclient.sensors.SensorGPSAndroid;
import it.isac.droidclient.sensors.SensorGPSfixUser;
import it.isac.droidclient.sensors.SensorIntValueUser;
import it.isac.droidclient.sensors.SensorMessageUser;
import it.isac.utils.impl.ComAdapterFactory;
import it.isac.utils.impl.SCMLogger;

public class MainActivity extends Activity {
	
	private AggregateFacilitator dev;
	
	private SensorGPSAndroid gpsDroid;
	private SensorGPSfixUser gpsFix;
	private SensorSource source1;
	private SensorSource source2;
	
	private TextView GPSlat;
	private TextView GPSlon;
	private TextView results;


	private LocationManager gpsManager;
	private LocationListener gpsListener;

	private EditText txtFixLon;
	private EditText txtFixLat;
	private TextView txtServerAddr;
	private TextView txtNetID;
	private Button btnSetGPS;
	private Button btnStart;
	private Button btnStop;
	private CheckBox ckbSens1;
	private CheckBox ckbSens2;
	private EditText txtIntVal;
	private Button btnSetInt;

	private SensorManager sensorManager;
	private SensorEventListener accelerometerListener;
	private Sensor accelerometer;
	private SensorMessageUser message;
	private SensorIntValueUser intval;



	
	@Override
	protected void onResume() {
		super.onResume();
		
		readSettings();
		updateGraphics();
		updateGPSview();  
		//TODO lasciare start oppure sistemare in modo che si riconosca active se il device � attivo.
		
	}


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		//Initialize system utilities
		SCMLogger.setLogger(new DroidLogger("OurLogger"));
		Engine.getInstance().getRegisteredConverters().add(new JacksonConverter());
		SCMLogger.log("Logger  Initialized");
		
		try{
			gpsManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		}catch(Exception e){
			SCMLogger.error("Error LocationManager "+e);
		}
		SCMLogger.log("Location Manager Initialized");
		
		try{
			sensorManager= (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		}catch(Exception e){
			SCMLogger.error("Error SensorManager "+e);
		}
		SCMLogger.log("Sensor Manager Initialized");
		
		
		//Bound activity to its graphic objects
		fetchGraphics();		
		
		//create front-end sensors (not the ones used for the actual SC device)
		setGPSsensor();  
		setAccelerometerSensor();
		
		//Read configuration
		readSettings();
		
		//Update graphics
		updateGraphics();

		//activate front-end sensors
		updateGPSview();    //i always have the current real GPS location
		//updateAccelerometerview()
		
		ckbSens1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
		       @Override
		       public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
		    	   DeviceParameters.BoolSens1=isChecked;
		    	   SCMLogger.log("BoolSens1 = "+DeviceParameters.BoolSens1);
		    	   if(source1!=null){
			    	   if (isChecked){
			    		   source1.setOn();
			    		   SCMLogger.log("Sensor 1 ON");
			    	   }else{
			    		   source1.setOff();
			    		   SCMLogger.log("Sensor 1 OFF");
			    	   }
		    	   }
		       }      
		   }); 
		
		ckbSens2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
		       public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
		    	   DeviceParameters.BoolSens2=isChecked;
		    	   SCMLogger.log("BoolSens2 = "+DeviceParameters.BoolSens2);
		    	   if(source2!=null){
			    	   if (isChecked){
			    		   source2.setOn();
			    		   SCMLogger.log("Sensor 2 ON");
			    	   }else{
			    		   source2.setOff();
			    		   SCMLogger.log("Sensor 2 OFF");
			    	   }
		    	   }
		       }  
		   }); 
		
		
		btnStart.setEnabled(true);
		btnStart.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				btnStart.setEnabled(false);
				createAggregator();
				btnStop.setEnabled(true);
			}
		});
		
		btnStop.setEnabled(false);
		btnStop.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(dev!=null)
				dev.stop();
				if(gpsDroid!=null){
					gpsDroid.stopGPS();
				}
				btnStart.setEnabled(true);
				btnStop.setEnabled(false);
			}
		});
		
		btnSetGPS.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				if(DeviceParameters.isFixGPS){
					try{
						DeviceParameters.fixLatY=Double.parseDouble(txtFixLat.getText().toString());
						DeviceParameters.fixLonX=Double.parseDouble(txtFixLon.getText().toString());
					}catch(Exception e){
						SCMLogger.log("Error in value");
					}
					
					if(gpsFix!=null){
						IPosition pos;
						if(DeviceParameters.isLatLon){
							pos=new LatLonPosition(
									DeviceParameters.fixLatY, 
									DeviceParameters.fixLonX);
						}else{
							pos=new XYPosition(
									DeviceParameters.fixLonX,
									DeviceParameters.fixLatY);
						}
						gpsFix.setPosition(pos);
	
					}
					updateGPSview();
					
				}
				
			}
		});

		btnSetInt.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				try{
					DeviceParameters.intValueSens=Integer.parseInt(txtIntVal.getText().toString());
				}catch(Exception e){
					SCMLogger.log("Error in value");
				}
				
				if(intval!=null){
					intval.setVal(DeviceParameters.intValueSens);

				}
			}
		});
		
	}
	
	private void updateGraphics() {
		results.setText("---");
		txtServerAddr.setText(DeviceParameters.ServerIpAddress+" : "+DeviceParameters.ServerPort);
		txtNetID.setText(DeviceParameters.NetworkID);
		txtFixLat.setText(DeviceParameters.fixLatY.toString());
		txtFixLon.setText(DeviceParameters.fixLonX.toString());
	
	}


	private void fetchGraphics(){
		//Connect graphic objects
		btnStart=(Button)findViewById(R.id.buttonStart);
		btnStop=(Button)findViewById(R.id.buttonStop);
		btnSetGPS=(Button)findViewById(R.id.buttonSetGPSVal);
		ckbSens1=(CheckBox) findViewById(R.id.checkBoxSens1); 
		ckbSens2=(CheckBox) findViewById(R.id.checkBoxSens2); 
		txtFixLat=(EditText) findViewById(R.id.EditTextGPSfixLatY);
		txtFixLon=(EditText) findViewById(R.id.EditTextGPSfixLonX);
		txtIntVal=(EditText) findViewById(R.id.EditTextIntValSens);
		btnSetInt=(Button)findViewById(R.id.ButtonIntValSens);
		
		txtServerAddr=(TextView) findViewById(R.id.textViewServerAddrValue);
		txtNetID=(TextView) findViewById(R.id.textViewNetIDValue);
		GPSlat=(TextView) findViewById(R.id.textViewGPSLatVal);
		GPSlon=(TextView) findViewById(R.id.TextViewGPSLonVal);
		results=(TextView) findViewById(R.id.textViewResults); 
	}

	

	
	private void updateGPSview() {
		if(DeviceParameters.isFixGPS){
			gpsManager.removeUpdates(gpsListener);
			txtFixLat.setEnabled(true);
			txtFixLon.setEnabled(true);
			GPSlat.setText(DeviceParameters.fixLatY.toString());
			GPSlon.setText(DeviceParameters.fixLonX.toString());
		}else{
			txtFixLat.setEnabled(false);
			txtFixLon.setEnabled(false);
			gpsManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, gpsListener);
		}
	}
	
	private void updateAccelerometerview(){
		sensorManager.registerListener(accelerometerListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
	}

	private void setGPSsensor() {
		gpsListener= new LocationListener() {
		    public void onLocationChanged(Location location) {
		    	SCMLogger.log("Location: "+location.getLatitude()+ " "+location.getLongitude());
				GPSlat.setText(""+location.getLatitude());
				GPSlon.setText(""+location.getLongitude());
		    }
		    public void onStatusChanged(String provider, int status, Bundle extras) {}
		    public void onProviderEnabled(String provider) {}
		    public void onProviderDisabled(String provider) {}
		};
	}
	
	private void setAccelerometerSensor(){
		accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		accelerometerListener = new SensorEventListener() {
			@Override
			public void onSensorChanged(SensorEvent event) {
				setResult(event.values[0]+" "+event.values[1]+" "+event.values[2]);
			}			
			@Override
			public void onAccuracyChanged(Sensor sensor, int accuracy) {}
		};
	}

	
	private void readSettings() {
		InputStream inputStream = null;
		
		try {
			inputStream = openFileInput(DeviceParameters.configFilename);
			SCMLogger.log("Data Loaded from current file (internal storage)");
		} catch (FileNotFoundException e) {
			try {
				inputStream = getAssets().open("clientConfigure.properties");
			} catch (IOException e1) {
				SCMLogger.log("Data Not loaded");
			}
			SCMLogger.log("Data Loaded from default file (assets)");
		}
		
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String line = null;
			while ((line = reader.readLine()) != null) {
				SCMLogger.log("Line: "+line);
				if (!line.startsWith("//")) { // comment, skip this line
					String val = line.split("=")[1].trim();// get the value
					if (line.contains("BaseUrl"))
						DeviceParameters.ServerIpAddress = val; // change ip
					if (line.contains("Port"))
						DeviceParameters.ServerPort = val; // add port
					if (line.contains("NetId"))
						DeviceParameters.NetworkID = val; // change net
					if (line.contains("ConnType"))
						DeviceParameters.ConnectionType = val;
					if (line.contains("FixedGPS"))
						DeviceParameters.isFixGPS = Boolean.parseBoolean(val);
					if (line.contains("LatLonGPS"))
						DeviceParameters.isLatLon = Boolean.parseBoolean(val);
					if (line.contains("Frequency"))
						DeviceParameters.RefreshPeriod = Integer.parseInt(val);
					if (line.contains("DefaultSens1"))
						DeviceParameters.BoolSens1 = Boolean.parseBoolean(val);
					if (line.contains("DefaultSens2"))
						DeviceParameters.BoolSens2 = Boolean.parseBoolean(val);
					if (line.contains("DafaultLat"))
						DeviceParameters.fixLatY = Double.parseDouble(val);
					if (line.contains("DafaultLon"))
						DeviceParameters.fixLonX = Double.parseDouble(val);
					if (line.contains("FunType"))
						DeviceParameters.FunType = Enum.valueOf(DeviceParameters.FunctionType.class, val);
				}
			}
		} catch (Exception e) {
			SCMLogger.error("Error while parsing configuration file: " + e);
		}
	}



	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			if(dev!=null)
			dev.stop();
			if(gpsDroid!=null){
				gpsDroid.stopGPS();
			}
			btnStart.setEnabled(true);
			btnStop.setEnabled(false);
			startActivity(new Intent(this, SettingsActivity.class));
			return true;
		}
		if (id == R.id.action_about){
			AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
			alertDialog.setTitle("About...");
			alertDialog.setMessage(" Rocca Lorenzo \n Montagna Pierluigi \n Bezzi Matteo");
			alertDialog.show();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
	private void createAggregatorSensors(){
		//Initialize GPS if enabled
		SCMLogger.log("Creating GPS sensor" );
		if(DeviceParameters.isFixGPS){
			gpsFix = new SensorGPSfixUser( "gpsAndroid", new LatLonPosition(
					DeviceParameters.fixLatY, 
					DeviceParameters.fixLonX));
		}else{
			gpsDroid = new SensorGPSAndroid("gpsAndroid", gpsManager);
			gpsDroid.activateGPS();
		}
		
		
		SCMLogger.log("Creating Source sensor" );
		source1 = new SensorSource("source");
		if (DeviceParameters.BoolSens1) {
			source1.setOn();
		}
		
		SCMLogger.log("creating Message sensor");
		message= new SensorMessageUser("message");
		message.setMessage("TestMessage");
		
		SCMLogger.log("creating ptobabiliy sensor");
		intval= new SensorIntValueUser("probability");
		intval.setVal(DeviceParameters.intValueSens);
	}

	
	
	private void createAggregator(){
		//Configure connection adapter
		ComAdapterFactory.setCAIstance(new CAImplAndroid());
		ComAdapterFactory.configComAdapterBaseUrl(DeviceParameters.ServerIpAddress, DeviceParameters.ServerPort);
		ComAdapterFactory.configComAdapterNetId(DeviceParameters.NetworkID);
		
		//create aggregator
		SCMLogger.log("Creating Device");	
		dev = new AggregateFacilitator((long) DeviceParameters.RefreshPeriod, new LatLonExecContext());
		
		
		//create sensors
		createAggregatorSensors();
		
		//add sensors to aggregator
		SCMLogger.log("Adding sensors to Device" );
		try {
			dev.addRealSensor(DeviceParameters.isFixGPS?gpsFix:gpsDroid);
		} catch (Exception e1) {
			SCMLogger.error("Error "+e1);
		}
		
		dev.addRealSensor(source1);
		dev.addRealSensor(message);
		dev.addRealSensor(intval);
		
		// create starting fields
		SCMLogger.log("Creating function "+ DeviceParameters.FunType.name());
		
		FieldCalculusFunction fcfunction = null;
		
		switch (DeviceParameters.FunType){
		case Gradient: 
			BasicNodeValue gradStart = new BasicNodeValue("distGrad", "inf");
			fcfunction = new GradientFunction(gradStart);
		break;
		case Test: 
			BasicNodeValue mockStart = new BasicNodeValue("aaa", "1");
			fcfunction = new FieldFunctionMock(mockStart);
		break;
		case Gossip:
			INodeValue gossStart = new BasicNodeValue("nodeGoss");
			fcfunction = new GossipFunction(gossStart);
		break;
		case MsgGradient:
			INodeValue gradmsgDistStart = new BasicNodeValue("distGrad", "inf");
			INodeValue gradmsgMsgStart = new BasicNodeValue("msgGrad", "empty");
			TupleValue gradmsgStart = new TupleValue("tupGrad");
			gradmsgStart.setValue(Arrays.asList(gradmsgDistStart,gradmsgMsgStart));
			fcfunction = new GradMsgFunction(gradmsgStart);
		break;
		case PointingGradient:
			INodeValue gradpointDistStart = new BasicNodeValue("distGrad", "inf");
			INodeValue gradpointMsgStart = new BasicNodeValue("msgGrad", "empty");
			TupleValue gradpointStart = new TupleValue("tupGrad");
			gradpointStart.setValue(Arrays.asList(gradpointDistStart,gradpointMsgStart));
			fcfunction = new GradPointingFunction(gradpointStart);
		break;
		}
		

		Observer observerDroid = new Observer() {
			@Override
			public void update(Observable o, Object arg) {
				try{			
					SCMLogger.log("Observer: notify change function value to "+arg);
					setResult("Function Value: "+(String) arg);
				}catch(Exception e){
					SCMLogger.error("Error in observer "+e);
				}
			}
		};
		dev.addField(fcfunction, observerDroid);
		
		SCMLogger.log("Starting Device" );
		dev.start();	
	}
	
	public void setResult(final String s){
		results.post(new Runnable() {
		    public void run() {
		    	results.setText(s);
		    } 
		});
	}
	
	
	

}
