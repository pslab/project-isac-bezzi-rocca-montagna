package it.isac.droidclient.activities;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceCategory;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.RingtonePreference;
import android.text.Layout;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import it.isac.droidclient.R;
import it.isac.droidclient.misc.DeviceParameters;
import it.isac.droidclient.misc.DeviceParameters.FunctionType;
import it.isac.utils.impl.SCMLogger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;

public class SettingsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_settings);
		
		Button apply=(Button) findViewById(R.id.buttonApply);
		
		final EditText ip= (EditText) findViewById(R.id.EditTextGPSfixLatY);
		final EditText port= (EditText) findViewById(R.id.ipPort);
		final EditText netid= (EditText) findViewById(R.id.netID);
		final EditText frequency= (EditText) findViewById(R.id.refreshFreq);
		final CheckBox gpsfix= (CheckBox) findViewById(R.id.checkFixGPS);
		final RadioButton xypos= (RadioButton) findViewById(R.id.coordinatesXY);
		final RadioButton latlonpos= (RadioButton) findViewById(R.id.coordinatesLatLon);
		final Spinner functype= (Spinner) findViewById(R.id.spinner1);
		
		
		
		try{
			gpsfix.setChecked(DeviceParameters.isFixGPS);
			ip.setText(DeviceParameters.ServerIpAddress);
			port.setText(DeviceParameters.ServerPort);
			netid.setText(DeviceParameters.NetworkID);
			frequency.setText(""+DeviceParameters.RefreshPeriod);
		
			xypos.setChecked(!DeviceParameters.isLatLon);
			latlonpos.setChecked(DeviceParameters.isLatLon);
			
			ArrayAdapter<FunctionType> adapter=new ArrayAdapter<DeviceParameters.FunctionType>
			(this, android.R.layout.simple_spinner_item, DeviceParameters.FunctionType.values());
			adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			functype.setAdapter(adapter);
			functype.setSelection(adapter.getPosition(DeviceParameters.FunType));
			
		}catch(Exception e){
			SCMLogger.log("Error: "+e);
		}
		
		
		
		
		apply.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String config="";		
				config+="ConnType = ";
				config+="Server"+"\n";  //hard coded for now
				config+="BaseUrl = ";
				config+=ip.getText().toString()+"\n";
				config+="Port = ";
				config+=port.getText().toString()+"\n";
				config+="NetId = ";
				config+=netid.getText().toString()+"\n";
				config+="Frequency = ";
				config+=frequency.getText().toString()+"\n";
				config+="FixedGPS = ";
				config+=gpsfix.isChecked()+"\n";
				config+="LatLonGPS = ";
				config+=latlonpos.isChecked()+"\n";
				config+="FunType = ";
				config+=functype.getSelectedItem().toString()+"\n";
		
				Toast toast = Toast.makeText(getApplicationContext(), config, Toast.LENGTH_LONG);
				toast.show();
	
				writeToFile(config);
				
				finish();
			}
		});
		
	}
	


	private void writeToFile(String data) {
	    try {
	        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(openFileOutput(DeviceParameters.configFilename, Context.MODE_PRIVATE));
	        outputStreamWriter.write(data);
	        outputStreamWriter.close();
	    }
	    catch (IOException e) {
	        SCMLogger.log("Exception " + "File write failed: " + e.toString());
	    } 
	}


	

}