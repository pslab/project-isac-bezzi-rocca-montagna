package it.isac.droidclient.test;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import it.isac.client.impl.device.AggregateFacilitator;
import it.isac.client.impl.device.FieldCalculusFunction;
import it.isac.client.impl.device.LatLonExecContext;
import it.isac.commons.interfaces.INodeValue;
import it.isac.commons.interfaces.ISensor;
import it.isac.commons.model.LatLonPosition;
import it.isac.commons.model.nodevalues.BasicNodeValue;
import it.isac.commons.model.nodevalues.TupleValue;
import it.isac.commons.model.sensors.SensorSource;
import it.isac.droidclient.functions.FieldFunctionMock;
import it.isac.droidclient.functions.GossipFunction;
import it.isac.droidclient.functions.GradMsgFunction;
import it.isac.droidclient.functions.GradPointingFunction;
import it.isac.droidclient.functions.GradientFunction;
import it.isac.droidclient.misc.CAImplAndroid;
import it.isac.droidclient.misc.DeviceParameters;
import it.isac.droidclient.sensors.SensorGPSfixUser;
import it.isac.droidclient.sensors.SensorGPSmoveUser;
import it.isac.droidclient.sensors.SensorIntValueUser;
import it.isac.droidclient.sensors.SensorMessageUser;
import it.isac.utils.impl.CAImplDesktop;
import it.isac.utils.impl.ComAdapterFactory;

public class ClientTester extends JFrame{

	
	private SensorGPSmoveUser gpsFix;
	private AggregateFacilitator dev;
	private SensorSource source1;
	private SensorMessageUser message;
	private SensorIntValueUser intval;
	private Long RefreshPeriod;
	private double fixLatY;
	private double fixLonX;
	private boolean isSource;
	private String stringMess;


	private static String ServerIP;
	private static String ServerPort;
	private static String NetworkID;
	
	private static void SetParams(String ip, String port, String netid){
		ServerIP=ip;
		ServerPort=port;
		NetworkID=netid;
	}
	
	public ClientTester(long RefreshPeriod, 
			double fixLatY, double fixLonX, 
			String stringMess, boolean isSource) {
		super(stringMess);
		this.RefreshPeriod=RefreshPeriod;
		this.fixLatY=fixLatY;
		this.fixLonX=fixLonX;
		this.stringMess=stringMess;
		this.isSource=isSource;
		this.dev=createAggregator();
		
		JButton btnUp=new JButton("Up");
		JButton btnDown=new JButton("Down");
		JButton btnLeft=new JButton("Left");
		JButton btnRight=new JButton("Right");
		final JCheckBox source= new JCheckBox("Source");
		source.setSelected(isSource);
		
		setAlwaysOnTop(true);
		
		ActionListener al=new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				String cmd=e.getActionCommand();
								
				if(cmd.equals("Up")){
					gpsFix.MoveUp();
				}else if(cmd.equals("Down")){
					gpsFix.MoveDown();
				}else if(cmd.equals("Left")){
					gpsFix.MoveLeft();
				}else if(cmd.equals("Right")){
					gpsFix.MoveRight();
				}else if(cmd.equals("Source")){
					if(source.isSelected()){
						source1.setOn();
					}else{
						source1.setOff();
					}
				}
				
			}
		};
		
		
		
		btnUp.addActionListener(al);
		btnDown.addActionListener(al);
		btnLeft.addActionListener(al);
		btnRight.addActionListener(al);
		source.addActionListener(al);
		
		JPanel commands=new JPanel();
		
		commands.add(btnUp);
		commands.add(btnDown);
		commands.add(btnLeft);
		commands.add(btnRight);
		commands.add(source);
		
		
		add(commands);
		pack();
		setVisible(true);
		
		
		addWindowListener(new WindowAdapter() {
		    @Override
		    public void windowClosing(WindowEvent windowEvent) {
		    	
		    	try {
		    		System.out.println("Closing, "+dev);
		    		if(dev!=null){
		    			dev.dispose();
		    		}
				} catch (Exception e) {
					e.printStackTrace();
				}
		    }
		});
		
	}


	private AggregateFacilitator createAggregator(){
				
	
		//Configure connection adapter
		ComAdapterFactory.setCAIstance(new CAImplDesktop());
		ComAdapterFactory.configComAdapterBaseUrl(ServerIP, ServerPort);
		ComAdapterFactory.configComAdapterNetId(NetworkID);
		
		
		
		//create aggregator
		System.out.println("Creating Device");	
		AggregateFacilitator dev = new AggregateFacilitator(RefreshPeriod, new LatLonExecContext());
		
		
		//create sensors
		gpsFix = new SensorGPSmoveUser( "gpsAndroid", new LatLonPosition(
				fixLatY, fixLonX));
		
		source1 = new SensorSource("source");
		if(isSource){
			source1.setOn();
		}else{
			source1.setOff();
		}
		
		message= new SensorMessageUser("message");
		message.setMessage(stringMess);
		
		intval= new SensorIntValueUser("probability");
		intval.setVal(90);
		
		//add sensors to aggregator
		dev.addRealSensor(gpsFix);
		dev.addRealSensor(source1);
		dev.addRealSensor(message);
		dev.addRealSensor(intval);
		
		// create starting fields
		FieldCalculusFunction fcfunction = null;
		
		switch (DeviceParameters.FunctionType.PointingGradient){
		case Gradient: 
			BasicNodeValue gradStart = new BasicNodeValue("distGrad", "inf");
			fcfunction = new GradientFunction(gradStart);
		break;
		case Test: 
			BasicNodeValue mockStart = new BasicNodeValue("aaa", "1");
			fcfunction = new FieldFunctionMock(mockStart);
		break;
		case Gossip:
			INodeValue gossStart = new BasicNodeValue("nodeGoss");
			fcfunction = new GossipFunction(gossStart);
		break;
		case MsgGradient:
			INodeValue gradmsgDistStart = new BasicNodeValue("distGrad", "inf");
			INodeValue gradmsgMsgStart = new BasicNodeValue("msgGrad", "empty");
			TupleValue gradmsgStart = new TupleValue("tupGrad");
			gradmsgStart.setValue(Arrays.asList(gradmsgDistStart,gradmsgMsgStart));
			fcfunction = new GradMsgFunction(gradmsgStart);
		break;
		case PointingGradient:
			INodeValue gradpointDistStart = new BasicNodeValue("distGrad", "inf");
			INodeValue gradpointMsgStart = new BasicNodeValue("msgGrad", "empty");
			TupleValue gradpointStart = new TupleValue("tupGrad");
			gradpointStart.setValue(Arrays.asList(gradpointDistStart,gradpointMsgStart));
			fcfunction = new GradPointingFunction(gradpointStart);
		break;
		}
		

		Observer observerDroid = new Observer() {

			@Override
			public void update(Observable o, Object arg) {
				try{
					setResult("Function Value: "+(String) arg);
				}catch(Exception e){
					System.out.println("Error in observer "+e);
				}
			}

		};
		dev.addField(fcfunction, observerDroid);
		
		System.out.println("Starting Device" );
		dev.start();
		
		return dev;
		
	}
	
	protected void setResult(String string) {
		System.out.println(string);
	}


	public static void main(String[] args) {
		ClientTester.SetParams("127.0.0.1", "8111", "net0");

		//Stazione ravenna 44.419028, 12.207468
		new ClientTester(1000, 44.419028d, 12.207468d, "Stazione", true);
		//Via di Roma 44.416597, 12.204273
		new ClientTester(1000, 44.416597d, 12.204273d, "Via di Roma", false);
		//Teatro Alighieri 44.417162, 12.201440
		new ClientTester(1000, 44.417162d, 12.201440d, "Teatro Alighieri", false);
		//Via Ghiselli 44.420852, 12.202371
		new ClientTester(1000, 44.420852d, 12.202371d, "Via Ghiselli", false);
		//Galla Placidia 44.420417, 12.196672
		new ClientTester(1000, 44.420417d, 12.196672d, "Galla Placidia", false);
		//Via Rampina 44.420107, 12.191518
		new ClientTester(1000, 44.420107d, 12.191518d, "Via Rampina", false);

	}
	
}
