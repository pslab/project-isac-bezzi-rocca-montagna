package it.isac.droidclient.functions;

import java.util.Map;

import it.isac.client.impl.device.FieldCalculusFunction;
import it.isac.commons.interfaces.INodeValue;
import it.isac.commons.interfaces.ISensorSnapshot;
import it.isac.commons.model.NodeState;
import it.isac.commons.model.nodevalues.BasicNodeValue;
import it.isac.commons.model.sensors.SensorType;

public class FieldFunctionMock extends FieldCalculusFunction {
	String startingValue;

	public FieldFunctionMock(INodeValue startValue) {
		super(startValue);
	}

	@Override
	public INodeValue compute(INodeValue localCurrentStates, Map<String, NodeState> nbrState,
			Map<String, ISensorSnapshot> localSensors) {
		
		int i = Integer.parseInt(localCurrentStates.getValue());
		i++;
		// With observer, System out no more!
		
		// Print everything
		// Logger.log("My Current value: ");
		// Logger.log("Id: " + localCurrentStates.getKey());
		// Logger.log("Value: " + localCurrentStates.getValue());
		// Logger.log();
		//
//		 String nbrVal ="NbrValue: ";
//		 for(String k : nbrState.keySet()) {
//			 nbrVal+="Key:"+k;
//			 nbrVal +=nbrState.get(k).toString()+"\n";
//		 }
//		 Logger.log(nbrVal);
		// Logger.log();
		//
		// Logger.log("Sensor Value: ");
		// for (String k : localSensors.keySet()) {
		// Logger.log("Key: " + k);
		// Logger.log("Id: " + localSensors.get(k).getSensorId());
		// Logger.log("Type: " + localSensors.get(k).getType());
		// Logger.log("Value: " + localSensors.get(k).getValue());
		// }
//		for (String k : localSensors.keySet()) {
//			if (localSensors.get(k).getType() == SensorType.GPS)
//				Logger.log("Position: " + localSensors.get(k).getValue());
//		}
		//Logger.log();
		return new BasicNodeValue(localCurrentStates.getKey(), i+"");
	}
}
