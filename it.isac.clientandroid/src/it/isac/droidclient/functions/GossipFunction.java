package it.isac.droidclient.functions;

import java.util.Arrays;
import java.util.Map;
import java.util.Random;

import it.isac.client.impl.device.Domain;
import it.isac.client.impl.device.ExecutionContext;
import it.isac.client.impl.device.FieldCalculusFunction;
import it.isac.commons.interfaces.INodeValue;
import it.isac.commons.interfaces.ISensorSnapshot;
import it.isac.commons.model.NodeState;
import it.isac.commons.model.nodevalues.BasicNodeValue;
import it.isac.commons.model.nodevalues.TupleValue;
import it.isac.utils.impl.SCMLogger;

public class GossipFunction extends FieldCalculusFunction {

	public GossipFunction(INodeValue startingValue) {
		super(startingValue);
	}

	@Override
	public INodeValue compute(INodeValue localCurrentStates,
			Map<String, NodeState> nbrState,
			Map<String, ISensorSnapshot> localSensors) {
		
		INodeValue nextValue = new BasicNodeValue("msgGoss");
		
		ISensorSnapshot valueP = null;
		ISensorSnapshot message =null;
		
		// find the source and message sensor
		for (String k : localSensors.keySet()) {
			if (localSensors.get(k) != null) {
				if (localSensors.get(k).getSensorId().equals("probability"))
					valueP = localSensors.get(k);
				if (localSensors.get(k).getSensorId().equals("message"))
					message = localSensors.get(k);
			} else {
				nextValue.setValue("Error");
				SCMLogger.log("Error");
			}
		}
		
		int intValue=Integer.parseInt(valueP.getValue());
		
		// get the context
		ExecutionContext<?> exec = null;
		try {
			exec = getExecutionContext();
		} catch (Exception e) {
			nextValue.setValue("Error");
			SCMLogger.log("Error");
			e.printStackTrace();
		}
		
		Random rand=new Random();
		int res;
		String EmptyMessage = "Empty";
		
		//By default broadcasting null message	
		nextValue.setValue(EmptyMessage); 
		
		if(intValue<0){  //i'm a source
			nextValue.setValue(message.getValue()); 
		
		}else{
			
		//Then each node broadcasting has a possibility to have the message forwarded
		for (String k : nbrState.keySet()) {
			if (k != exec.getNodeId()) {
				NodeState ns = nbrState.get(k);
				for (INodeValue v : ns.getValues()) {
					if (v.getKey().equals("msgGoss")) { // find the correct value
						res=rand.nextInt(100);
						if (res>intValue && !v.getValue().equals(EmptyMessage))
							nextValue.setValue(v.getValue());  					}
				}
			}
		}
		
		}
		return nextValue;
	}
	
	
}
