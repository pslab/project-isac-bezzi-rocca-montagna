package it.isac.droidclient.functions;

import java.util.Arrays;
import java.util.Map;


import it.isac.client.impl.device.Domain;
import it.isac.client.impl.device.ExecutionContext;
import it.isac.client.impl.device.FieldCalculusFunction;
import it.isac.commons.interfaces.INodeValue;
import it.isac.commons.interfaces.ISensorSnapshot;
import it.isac.commons.model.NodeState;
import it.isac.commons.model.nodevalues.BasicNodeValue;
import it.isac.commons.model.nodevalues.TupleValue;
import it.isac.utils.impl.SCMLogger;

public class GradMsgFunction extends FieldCalculusFunction {

	public GradMsgFunction(INodeValue startingValue) {
		super(startingValue);
	}

	@Override
	public INodeValue compute(INodeValue localCurrentStates,
			Map<String, NodeState> nbrState,
			Map<String, ISensorSnapshot> localSensors) {
		
		TupleValue nextTupleValue = new TupleValue(localCurrentStates.getKey()); 
		INodeValue nextValue = new BasicNodeValue("distGrad");
		INodeValue nextMsg = new BasicNodeValue("msgGrad");
		
		ISensorSnapshot source = null;
		ISensorSnapshot message =null;
		
		// find the source and message sensor
		for (String k : localSensors.keySet()) {
			if (localSensors.get(k) != null) {
				if (localSensors.get(k).getSensorId().equals("source"))
					source = localSensors.get(k);
				if (localSensors.get(k).getSensorId().equals("message"))
					message = localSensors.get(k);
			} else {
				nextValue.setValue("Error");
				SCMLogger.log("Error");
			}
		}
		
		// get the context
		ExecutionContext<?> exec = null;
		try {
			exec = getExecutionContext();
		} catch (Exception e) {
			nextValue.setValue("Error");
			SCMLogger.log("Error");
			e.printStackTrace();
		}
		
		//Evaluate
		if (Boolean.parseBoolean(source.getValue())) {			// is source
			nextValue.setValue("0"); // distance 0
			nextMsg.setValue(message.getValue());
		
		} else { 												// isn't source
			String keyMinNbr = "";
			String keyMinMsg = "nbr";
			double minDist = Double.MAX_VALUE;
			// find the node with min distance
			for (String k : nbrState.keySet()) {
				if (k != exec.getNodeId()) {
					NodeState ns = nbrState.get(k);
					double dist = Double.MAX_VALUE;
					String msg = "n";
					
					for (INodeValue v : ns.getValues()) {
						if (v.getKey().equals("tupGrad")) { // find the correct value
							try {
								String distStr= v.getValue(); //take tuple value
								distStr=distStr.substring(1,distStr.length()-1); //remove < >
								String[] values = distStr.split(", "); //tokenize
								
								dist = Double.parseDouble(values[0]);
								msg = values[1];
							} catch (Exception ex) {
								dist = Double.MAX_VALUE;
								//SCMLogger.error(""+ex);
							}
							SCMLogger.log(k + " gradientVal: " + v.getValue());
						}
					}
					if (dist < minDist) {
						//SCMLogger.log(exec.getNodeId()+ " found a new min: " + dist);
						minDist = dist;
						keyMinNbr = k;
						keyMinMsg = msg;
						
					}
				}
			}
						
			if (keyMinNbr.isEmpty()){
				nextValue.setValue("inf");
				SCMLogger.log("setValue('inf')");
				nextMsg.setValue("none");
			}else {
				if (exec != null)
					nextValue.setValue(Double.toString(minDist + exec.distanceTo(keyMinNbr)));
				nextMsg.setValue(keyMinMsg);
			}
		}
		
		//compose result
		nextTupleValue.setValue(Arrays.asList(nextValue,nextMsg));
		
		return nextTupleValue;
	}
}
