package it.isac.droidclient.functions;

import java.util.Arrays;
import java.util.Map;


import it.isac.client.impl.device.Domain;
import it.isac.client.impl.device.ExecutionContext;
import it.isac.client.impl.device.FieldCalculusFunction;
import it.isac.commons.interfaces.INodeValue;
import it.isac.commons.interfaces.IPosition;
import it.isac.commons.interfaces.ISensorSnapshot;
import it.isac.commons.model.LatLonPosition;
import it.isac.commons.model.NodeState;
import it.isac.commons.model.nodevalues.BasicNodeValue;
import it.isac.commons.model.nodevalues.TupleValue;
import it.isac.utils.impl.SCMLogger;

public class GradPointingFunction extends FieldCalculusFunction {

	public GradPointingFunction(INodeValue startingValue) {
		super(startingValue);
	}

	@Override
	public INodeValue compute(INodeValue localCurrentStates,
			Map<String, NodeState> nbrState,
			Map<String, ISensorSnapshot> localSensors) {
		
		INodeValue nextTupleValue = new BasicNodeValue(localCurrentStates.getKey()); 
		
		INodeValue nextValue = new BasicNodeValue("distGrad");
		INodeValue nextMsg = new BasicNodeValue("msgGrad");
		
		ISensorSnapshot source = null;
		
		// find the source and message sensor
		for (String k : localSensors.keySet()) {
			if (localSensors.get(k) != null) {
				if (localSensors.get(k).getSensorId().equals("source"))
					source = localSensors.get(k);
			} else {
				nextValue.setValue("Error");
				SCMLogger.log("Error");
			}
		}
		
		// get the context
		ExecutionContext<?> exec = null;
		try {
			exec = getExecutionContext();
		} catch (Exception e) {
			nextValue.setValue("Error");
			SCMLogger.log("Error");
			e.printStackTrace();
		}
		
		//Evaluate
		if (Boolean.parseBoolean(source.getValue())) {			// is source
			nextValue.setValue("0"); // distance 0
			LatLonPosition p=(LatLonPosition) exec.getDevicePosition();
			nextMsg.setValue(""+p.getLat()+", "+p.getLon());	
		} else { 												// isn't source
			String keyMinNbr = "";
			String keyMinMsg = "";
			double minDist = Double.MAX_VALUE;
			// find the node with min distance
			for (String k : nbrState.keySet()) {
				if (!k.equals(exec.getNodeId())) {
					NodeState ns = nbrState.get(k);
					double dist = Double.MAX_VALUE;
					String msg = "";
					//To point at the source
					for (INodeValue v : ns.getValues()) {
						if (v.getKey().equals("tupGrad")) { // find the correct value
							try {
								String distStr= v.getValue(); //take tuple value
								distStr=distStr.substring(1,distStr.length()-1); //remove < >
								String[] values = distStr.split(", "); //tokenize
								dist = Double.parseDouble(values[0]);
								if(values.length==3)
									msg = values[1]+", "+values[2]; 
							} catch (Exception ex) {
								dist = Double.MAX_VALUE;
							}
						}
					}
					if (dist < minDist) {
						minDist = dist;
						keyMinNbr = k;
						keyMinMsg = msg;		
					}
				}
			}
						
			if (keyMinNbr.isEmpty()){
				nextValue.setValue("inf");
				nextMsg.setValue("noSource");
			}else {
				if (exec != null)
					nextValue.setValue(Double.toString(minDist + exec.distanceTo(keyMinNbr)));
				nextMsg.setValue(keyMinMsg);
			}
		}
		
		nextTupleValue.setValue("<"+nextValue.getValue()+", "+nextMsg.getValue()+">");
		
		return nextTupleValue;
	}
}
