package it.isac.droidclient.sensors;

import java.util.Random;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import it.isac.commons.interfaces.IPosition;
import it.isac.commons.interfaces.ISensor;
import it.isac.commons.interfaces.ISensorSnapshot;
import it.isac.commons.model.LatLonPosition;
import it.isac.commons.model.PositionType;
import it.isac.commons.model.XYPosition;
import it.isac.commons.model.sensors.BaseSensorSnapshot;
import it.isac.commons.model.sensors.SensorType;

public class SensorGPSAndroid implements ISensor {

	String id;
	IPosition position;
	
	private LocationManager locationManager;
	private LocationListener locationListener;

	public SensorGPSAndroid(String id, LocationManager gpsService) {
		this.id = id;
		this.locationManager=gpsService;
		position = new LatLonPosition(0, 0);		
		init();
	}
	
	private void init() {
		locationListener = new LocationListener() {		    
				public void onLocationChanged(Location location) {
					//Utils.log("Location: "+location.getLatitude()+ " "+location.getLongitude());

			    	((LatLonPosition) position).setLat(location.getLatitude());
					((LatLonPosition) position).setLon(location.getLongitude());
			    }
				
			    public void onStatusChanged(String provider, int status, Bundle extras) {}
			    public void onProviderEnabled(String provider) {}
			    public void onProviderDisabled(String provider) {}
			  };
	}
	
	// Control Methods
	public void activateGPS() {
		// Register the listener with the Location Manager to receive location updates
		locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, locationListener);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, locationListener);
	}

	public void stopGPS() {
		locationManager.removeUpdates(locationListener);
	}

	@Override
	public ISensorSnapshot getValue() {
		return new BaseSensorSnapshot(id, getType(), position.toString());
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getType() {
		return SensorType.GPS;
	}
	


}
