package it.isac.droidclient.sensors;

import it.isac.commons.interfaces.ISensor;
import it.isac.commons.interfaces.ISensorSnapshot;
import it.isac.commons.model.sensors.BaseSensorSnapshot;
import it.isac.commons.model.sensors.SensorType;

public class SensorMessageUser implements ISensor {

	String id;
	String message = "";
	
	public SensorMessageUser(String sensorId) {
		this.id = sensorId;
	}
	
	public void setMessage(String m) {
		message = m;
	}

	@Override
	public ISensorSnapshot getValue() {
		return new BaseSensorSnapshot(getId(), getType(), message);
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getType() {
		return SensorType.PAYLOAD;
	}
}
