package it.isac.droidclient.sensors;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import it.isac.commons.interfaces.ISensor;
import it.isac.commons.interfaces.ISensorSnapshot;
import it.isac.commons.model.sensors.BaseSensorSnapshot;
import it.isac.commons.model.sensors.SensorType;

public class SensorAccelAndroid implements ISensor {

	private SensorManager sensorManager;
	private Sensor accelerometer;
	private SensorEventListener accelerometerListener;
	private String id;
	private Float[] accVector;

	public SensorAccelAndroid(String id, SensorManager mSensorManager) {
		this.sensorManager = mSensorManager;
		this.id=id;
		accVector=new Float[] {0f,0f,0f};
		accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		accelerometerListener = new SensorEventListener() {
			
			@Override
			public void onSensorChanged(SensorEvent event) {
				accVector[0]=event.values[0];
				accVector[1]=event.values[1];
				accVector[2]=event.values[2];				
			}
			
			@Override
			public void onAccuracyChanged(Sensor sensor, int accuracy) {
				// TODO Auto-generated method stub
				
			}
		};
		
		

	}
	
	public void activateAccelerometer(){
		sensorManager.registerListener(accelerometerListener, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
	}
	
	public void stopAccelerometer(){
		sensorManager.unregisterListener(accelerometerListener);
	}
	
	
	@Override
	public ISensorSnapshot getValue() {
		return new BaseSensorSnapshot(id, getType(), "<"+accVector[0]+", "+accVector[1]+", "+accVector[2]+">");
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getType() {
		return SensorType.ACCELERATION;
	}

}
