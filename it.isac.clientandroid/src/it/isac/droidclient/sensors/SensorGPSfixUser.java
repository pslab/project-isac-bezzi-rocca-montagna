package it.isac.droidclient.sensors;

import android.text.Editable;
import it.isac.commons.interfaces.IPosition;
import it.isac.commons.interfaces.ISensor;
import it.isac.commons.interfaces.ISensorSnapshot;
import it.isac.commons.model.sensors.BaseSensorSnapshot;
import it.isac.commons.model.sensors.SensorType;

public class SensorGPSfixUser implements ISensor {
	String id;
	IPosition position;
	
	
	public SensorGPSfixUser(String id,	IPosition position) {
		this.id=id;
		this.position=position;
	}
	
	@Override
	public ISensorSnapshot getValue() {
		return new BaseSensorSnapshot(id, getType(), position.toString());
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getType() {
		return SensorType.GPS;
	}
	
	public synchronized void  setPosition(IPosition position){
		this.position=position;
	}



}
