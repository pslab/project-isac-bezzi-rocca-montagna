package it.isac.droidclient.sensors;

import it.isac.commons.interfaces.ISensor;
import it.isac.commons.interfaces.ISensorSnapshot;
import it.isac.commons.model.sensors.BaseSensorSnapshot;
import it.isac.commons.model.sensors.SensorType;

public class SensorIntValueUser implements ISensor {

	String id;
	int value = 0;
	
	public SensorIntValueUser(String sensorId) {
		this.id = sensorId;
	}
	
	public void setVal(int n) {
		value = n;
	}
	public void reset() {
		value = 0;
	}

	@Override
	public ISensorSnapshot getValue() {
		return new BaseSensorSnapshot(getId(), getType(), Integer.toString(value));
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getType() {
		return SensorType.INT_VALUE;
	}
}
