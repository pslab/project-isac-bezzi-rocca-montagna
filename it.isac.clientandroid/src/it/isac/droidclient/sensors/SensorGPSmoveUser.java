package it.isac.droidclient.sensors;

import android.text.Editable;
import it.isac.commons.interfaces.IPosition;
import it.isac.commons.interfaces.ISensor;
import it.isac.commons.interfaces.ISensorSnapshot;
import it.isac.commons.model.LatLonPosition;
import it.isac.commons.model.sensors.BaseSensorSnapshot;
import it.isac.commons.model.sensors.SensorType;

public class SensorGPSmoveUser implements ISensor {
	String id;
	IPosition position;
	Double lat,lon;
	Double step= 0.0001;
	
	public SensorGPSmoveUser(String id,	IPosition position) {
		this.id=id;
		this.position=position;
		lat=((LatLonPosition)position).getLat();
		lon=((LatLonPosition)position).getLon();
	}
	
	@Override
	public ISensorSnapshot getValue() {
		return new BaseSensorSnapshot(id, getType(), position.toString());
	}
	
	
	
	public void MoveUp(){
		lat+=step;
		((LatLonPosition)position).setLat(lat);
	}
	public void MoveDown(){
		lat-=step;
		((LatLonPosition)position).setLat(lat);
	}
	public void MoveLeft(){
		lon-=step;
		((LatLonPosition)position).setLon(lon);
	}
	public void MoveRight(){
		lon+=step;
		((LatLonPosition)position).setLon(lon);
	}
	

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getType() {
		return SensorType.GPS;
	}
	
	public synchronized void  setPosition(IPosition position){
		this.position=position;
	}



}
