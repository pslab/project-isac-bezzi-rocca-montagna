package it.isac.droidclient.misc;

public class DeviceParameters {
	public static final String configFilename = "config.ini";
	
	//Network
	public static String ServerIpAddress="192.168.1.10";
	public static String ServerPort="8111";
	public static String NetworkID="net0";
	
	//GPS
	public static Boolean isFixGPS=false;
	
	//Performances
	public static int RefreshPeriod=1000;
	
	
	//Sensor Values
	public static Boolean BoolSens1=false;
	public static Double fixLatY=44.4194877d;
	public static Double fixLonX=12.1956804d;


}

