package it.isac.droidclient.compass.activities;

import java.util.Arrays;
import java.util.Observable;
import java.util.Observer;

import org.restlet.engine.Engine;
import org.restlet.ext.jackson.JacksonConverter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import it.isac.client.impl.device.AggregateFacilitator;
import it.isac.client.impl.device.FieldCalculusFunction;
import it.isac.client.impl.device.LatLonExecContext;
import it.isac.clientandroid.compass.R;
import it.isac.commons.interfaces.INodeValue;
import it.isac.commons.model.LatLonPosition;
import it.isac.commons.model.nodevalues.BasicNodeValue;
import it.isac.commons.model.nodevalues.TupleValue;
import it.isac.commons.model.sensors.SensorSource;
import it.isac.droidclient.compass.functions.GradPointingFunction;
import it.isac.droidclient.compass.sensors.SensorGPSAndroid;
import it.isac.droidclient.compass.sensors.SensorGPSfixUser;
import it.isac.droidclient.misc.CAImplAndroid;
import it.isac.droidclient.misc.DeviceParameters;
import it.isac.droidclient.misc.DroidLogger;
import it.isac.utils.impl.ComAdapterFactory;
import it.isac.utils.impl.SCMLogger;
import android.app.Activity;
import android.hardware.GeomagneticField;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements SensorEventListener, LocationListener {

	//App Graphics
	private ImageView image;
	private TextView tvHeading;
	private CheckBox cbIsSource;
	private TextView tvResults;
	
	//App Sensors
	private SensorManager mSensorManager;
	private LocationManager mLocationManger;
	private Sensor accelerometer;
	private Sensor magnetometer;
	private Location loc; // Will hold last known location
	private Location wptLoc;
	
	//App variables
	private boolean isCompassActive;
	private float currentDegree = 0f;
	private float azimuth = 0f; // View to draw a compass
	private float dist = -1;
	private float bearing = 0;
	private float heading = 0;
	private float arrow_rotation = 0;
	private float[] mGravity;
	private float[] mGeomagnetic;
	

	//SCM
	private AggregateFacilitator dev;
	private SensorSource source1;
	private SensorGPSfixUser gpsFix;
	private SensorGPSAndroid gpsDroid;
	private SharedPreferences preferences;
	private TextView tvGps;
	
	
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		SCMLogger.setLogger(new DroidLogger("OurLogger"));
		Engine.getInstance().getRegisteredConverters().add(new JacksonConverter());
		SCMLogger.log(" ---- On Create ---- ");
		//To keep the screen on
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
		//Manage settings
		preferences = getSharedPreferences("prefName", MODE_PRIVATE);
		
		DeviceParameters.ServerIpAddress=preferences.getString("IP_Address", "192.168.0.101");
		DeviceParameters.ServerPort=preferences.getString("Port", "8111");
		DeviceParameters.NetworkID=preferences.getString("NetID", "net0");
		DeviceParameters.RefreshPeriod=(int) preferences.getLong("Period", 1000);
		
		
		//get graphics elements
		image = (ImageView) findViewById(R.id.imageViewCompass);
		tvHeading = (TextView) findViewById(R.id.tvHeading);
		tvResults = (TextView) findViewById(R.id.tvResults);
		cbIsSource = (CheckBox) findViewById(R.id.cbSource);
		tvGps=(TextView) findViewById(R.id.tvGpsStatus);

		// initialize your android device sensor capabilities
		mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
		mLocationManger = (LocationManager) getSystemService(LOCATION_SERVICE);

		// initialize app sensors
		accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
		magnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
		
		String provider = mLocationManger.getBestProvider(new Criteria(), true);
		loc = mLocationManger.getLastKnownLocation(provider);
		
		startCompass();
		createAggregator();
	
		cbIsSource.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked){
					//is source
					source1.setOn();
					stopCompass();
					DeviceParameters.BoolSens1=true;
				}else{
					//isNot
					source1.setOff();
					resumeCompass();
					DeviceParameters.BoolSens1=false;
				}				
			}
		});
		
		image.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String loca;
				if(loc!=null){
					loca="My Position: "+
					"\n"+loc.getLongitude()+
					"\n"+ loc.getLatitude()+
					"\n"+"Range: " + loc.getAccuracy()+
					"\n"+"Provider: "+loc.getProvider();
					if(wptLoc!=null){
						loca+="\n"+"\n"+"Source Position: "+
								"\n"+wptLoc.getLongitude()+
								"\n"+ wptLoc.getLatitude()+
								//"\n"+"Range: " + wptLoc.getAccuracy()+
								//" Provider: "+wptLoc.getProvider()+
								"\n"+"Distance: "+loc.distanceTo(wptLoc);
								
					}
					
				}else{
					loca = "No location available";
				}
				Toast toast = Toast.makeText(getBaseContext(),loca,Toast.LENGTH_SHORT);
				toast.show();
			}
		});
	}
	
	
	private void startCompass(){
		isCompassActive = true;
		selectCorrectImage();
		mSensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
		mSensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_GAME);
		mLocationManger.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);
		//mLocationManger.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
	}
	
	private void resumeCompass(){
		isCompassActive = true;
		selectCorrectImage();
	}
	
	private void stopCompass(){
		isCompassActive = false;
		selectCorrectImage();
	}
	
	private void setCompassTarget(Double lat, Double lon, String provider){
		if(wptLoc==null)
			wptLoc= new Location("");
		wptLoc.setLongitude(lon);
		wptLoc.setLatitude(lat);
		wptLoc.setProvider(provider);
	}
	
	private void resetCompass(){
		wptLoc=null;
	}
		
	protected void onResume() {
		super.onResume();
		SCMLogger.log(" ---- On Resume ---- ");
		if(DeviceParameters.BoolSens1){
	        cbIsSource.setChecked(true);
	        stopCompass();
        }else{
        	resumeCompass();
        }
	}

	protected void onPause() {
		super.onPause();
		SCMLogger.log(" ---- On Pause ---- ");
	}
	
	protected void onStop(){
		super.onStop();
		SCMLogger.log(" ---- On Stop ---- ");
	}
	
	protected void onDestroy(){
		super.onDestroy();
		SCMLogger.log(" ---- On Destroy ---- ");
		
		//stop the aggregator facilitator
		dev.dispose();
		//remove listeners
		mSensorManager.unregisterListener(this);
		mLocationManger.removeUpdates(this);
	}
	


	public void onAccuracyChanged(Sensor sensor, int accuracy) {
	}

	public void onSensorChanged(SensorEvent event) {
		String res="";
		
		//Get the azimuth
		if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
			mGravity = event.values;
		}
		if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
			mGeomagnetic = event.values;
		}
		if (mGravity != null && mGeomagnetic != null) {
			float R[] = new float[9];
			float I[] = new float[9];
			boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
			if (success) {
				float orientation[] = new float[3]; 
				SensorManager.getOrientation(R, orientation);
				azimuth = orientation[0]; // orientation: [0]=azimuth (-pi to pi), [1]=pitch (-pi/2 to pi/2), [2]=roll (-pi to pi)
			}
		}
		res+="Azimuth magnetic rad: "+azimuth+"\n";
		
		//Convert azimuth to degree
		float azimuthDeg = azimuth * 360 / (2 * 3.14159f);
		res+="Azimuth magnetic deg: "+azimuthDeg+"\n";
		float azimuthDeg360 = azimuthDeg>0?azimuthDeg:((180+azimuthDeg)+180);
		res+="Azimuth360 magnetic deg: "+azimuthDeg360+"\n";
		
	    //Set the heading field
	    String bearingText = " ";

	    if ( (360 >= azimuthDeg360 && azimuthDeg360 >= 337.5) || (0 <= azimuthDeg360 && azimuthDeg360 <= 22.5) ) bearingText = "N";
	    else if (azimuthDeg360 > 22.5 && azimuthDeg360 < 67.5) bearingText = "NE";
	    else if (azimuthDeg360 >= 67.5 && azimuthDeg360 <= 112.5) bearingText = "E";
	    else if (azimuthDeg360 > 112.5 && azimuthDeg360 < 157.5) bearingText = "SE";
	    else if (azimuthDeg360 >= 157.5 && azimuthDeg360 <= 202.5) bearingText = "S";
	    else if (azimuthDeg360 > 202.5 && azimuthDeg360 < 247.5) bearingText = "SW";
	    else if (azimuthDeg360 >= 247.5 && azimuthDeg360 <= 292.5) bearingText = "W";
	    else if (azimuthDeg360 > 292.5 && azimuthDeg360 < 337.5) bearingText = "NW";
	    else bearingText = " ";

	    //Update heading
	    tvHeading.setText(bearingText);
		
	    selectCorrectImage();
	    
		if (wptLoc!= null && loc!=null && isCompassActive){
		
			float newBearing=0;
			float newDeclination=0;
			
			try {
				dist = loc.distanceTo(wptLoc);
				bearing = loc.bearingTo(wptLoc); // -180 to 180
				heading = loc.getBearing(); // 0 to 360
				
				arrow_rotation = (360 + ((bearing + 360) % 360) - heading) % 360;  // -180 to 180
				
				res+="declination deg: "+newDeclination+"\n";
				newBearing = loc.bearingTo(wptLoc);
				res+="bearing deg: "+newBearing+"\n";
	
			}catch(Exception e){
				SCMLogger.error("error " + e);
			}
			
			// Show movement of Arrow 
			rotateArrow(azimuthDeg360-newBearing);
		}

	}
	
	private void selectCorrectImage(){
		if(!isCompassActive){
			image.setImageResource(R.drawable.exc_mark);
			resetArrowRotation();		
		}else if(loc!=null && wptLoc!=null){
			image.setImageResource(R.drawable.location_arrow);
		}else{
			image.setImageResource(R.drawable.droid);
			resetArrowRotation();	
		}
	}
	
	
	private void rotateArrow(float nextDegree){
			
		// create a rotation animation (reverse turn degree degrees)
		RotateAnimation ra = new RotateAnimation(currentDegree, -nextDegree, Animation.RELATIVE_TO_SELF, 0.5f,
				Animation.RELATIVE_TO_SELF, 0.5f);

		// how long the animation will take place
		ra.setDuration(500);

		// set the animation after the end of the reservation status
		ra.setFillAfter(true);

		// Start the animation
		image.startAnimation(ra);
		currentDegree = -nextDegree;		

	}
	
	public void resetArrowRotation(){
		//with animation
			rotateArrow(0);	
		//sudden
			//image.setRotation(-currentDegree);
			//currentDegree=0;	
	}

	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle("Settings");

			// Set up the input
			final LinearLayout settingslo= new LinearLayout(this);
			settingslo.setOrientation(LinearLayout.VERTICAL);
			final EditText ipInput = new EditText(this);
			final EditText portInput = new EditText(this);
			final EditText netidInput = new EditText(this);
			final EditText periodInput = new EditText(this);
			final TextView ipLabel = new TextView(this);
			final TextView portLabel = new TextView(this);
			final TextView netidLabel = new TextView(this);
			final TextView periodLabel = new TextView(this);
			
			ipInput.setText(DeviceParameters.ServerIpAddress);
			ipInput.setInputType(InputType.TYPE_CLASS_PHONE);
			portInput.setText(DeviceParameters.ServerPort);
			portInput.setInputType(InputType.TYPE_CLASS_NUMBER);
			netidInput.setText(DeviceParameters.NetworkID);
			periodInput.setText(DeviceParameters.RefreshPeriod+"");
			periodInput.setInputType(InputType.TYPE_CLASS_NUMBER);
			ipLabel.setText(" Server IP");
			portLabel.setText(" port");
			netidLabel.setText(" netId");
			periodLabel.setText(" Refresh period");
			
			settingslo.addView(ipLabel);
			settingslo.addView(ipInput);
			
			settingslo.addView(portLabel);
			settingslo.addView(portInput);
			
			settingslo.addView(netidLabel);
			settingslo.addView(netidInput);
			
			settingslo.addView(periodLabel);
			settingslo.addView(periodInput);
			
			builder.setView(settingslo);

			// Set up the buttons
			builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { 
			    @Override
			    public void onClick(DialogInterface dialog, int which) {
			        //Set parameters for current run
			    	DeviceParameters.ServerIpAddress = ipInput.getText().toString();
			        DeviceParameters.ServerPort=portInput.getText().toString();
			        DeviceParameters.NetworkID=netidInput.getText().toString();
			        DeviceParameters.RefreshPeriod=Integer.parseInt(periodInput.getText().toString());
			        
			        //Save parameters
			        SharedPreferences.Editor edit= preferences.edit();

			        edit.putString("IP_Address", DeviceParameters.ServerIpAddress );
			        edit.putString("Port", DeviceParameters.ServerPort );
			        edit.putString("NetID", DeviceParameters.NetworkID );
			        edit.putLong("Period", DeviceParameters.RefreshPeriod );
			        edit.commit();

			        
			        if(dev!=null){
			        	dev.dispose();
			        }
			        
			        resetCompass();
			        createAggregator();
			        
			        if(DeviceParameters.BoolSens1){
				        cbIsSource.setChecked(true);
				        stopCompass();
			        }else{
			        	startCompass();
			        }
			        
			    }
			});
			builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			    @Override
			    public void onClick(DialogInterface dialog, int which) {
			        dialog.cancel();
			    }
			});

			builder.show();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onLocationChanged(Location location) {
		this.loc = location;
//		String loca="Position: "+location.getLongitude()+
//				", "+ location.getLatitude()+
//				"\n"+"Range: " + location.getAccuracy()+
//				" Provider: "+location.getProvider();
//		tvGps.setText(loca);
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}
	
	private void createAggregator(){
		//Configure connection adapter
		ComAdapterFactory.setCAIstance(new CAImplAndroid());
		ComAdapterFactory.configComAdapterBaseUrl(DeviceParameters.ServerIpAddress, DeviceParameters.ServerPort);
		ComAdapterFactory.configComAdapterNetId(DeviceParameters.NetworkID);
		
		//create aggregator
		SCMLogger.log("Creating Device");	
		dev = new AggregateFacilitator((long) DeviceParameters.RefreshPeriod, new LatLonExecContext());
		
		
		
		
		
		SCMLogger.log("Creating GPS sensor" );
		if(DeviceParameters.isFixGPS){
			gpsFix = new SensorGPSfixUser( "gpsAndroid", new LatLonPosition(
					DeviceParameters.fixLatY, 
					DeviceParameters.fixLonX));
		}else{
			gpsDroid = new SensorGPSAndroid("gpsAndroid",mLocationManger );
			gpsDroid.activateGPS();
		}
		
		SCMLogger.log("Creating Source sensor" );	
		source1 = new SensorSource("source");
		if (DeviceParameters.BoolSens1) {
			source1.setOn();
		}
		
		//add sensors to aggregator
		SCMLogger.log("Adding sensors to Device" );
		

		try {
			dev.addRealSensor(DeviceParameters.isFixGPS?gpsFix:gpsDroid);
		} catch (Exception e1) {
			SCMLogger.error("Error "+e1);
		}
		
		dev.addRealSensor(source1);
		
		// create starting fields
		FieldCalculusFunction fcfunction = null;
		

			INodeValue gradpointDistStart = new BasicNodeValue("distGrad", "inf");
			INodeValue gradpointMsgStart = new BasicNodeValue("msgGrad", "empty");
			TupleValue gradpointStart = new TupleValue("tupGrad");
			gradpointStart.setValue(Arrays.asList(gradpointDistStart,gradpointMsgStart));
			fcfunction = new GradPointingFunction(gradpointStart);

		

		Observer observerDroid = new Observer() {

			@Override
			public void update(Observable o, Object arg) {
				try{
					//TODO gestire qui la perdita di connettivitą?
					//TODO mettere il try catch nella chiamata agli observers?
						
					SCMLogger.log("Observer: notify change function value to "+arg);
					setResult("Function Value: "+(String) arg);
					
					String val=(String)arg;
					val=val.substring(1,val.length()-1);
					String[] values = val.split(", "); //tokenize
					if(values.length==3){
						setCompassTarget(Double.parseDouble(values[1]), Double.parseDouble(values[2]),"SCM");
					}else{
						resetCompass();
					}
				}catch(Exception e){
					SCMLogger.error("Error in observer "+e);
				}
			}
		};
		dev.addField(fcfunction, observerDroid);
		
		SCMLogger.log("Starting Device" );
		dev.start();
		
	}
	
	public void setResult(final String s){
		tvResults.post(new Runnable() {
		    public void run() {
		    	tvResults.setText(s);
		    } 
		});
	}
	
	
}
