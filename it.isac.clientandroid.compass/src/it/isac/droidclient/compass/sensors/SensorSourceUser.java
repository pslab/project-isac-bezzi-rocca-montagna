package it.isac.droidclient.compass.sensors;

import it.isac.commons.interfaces.ISensor;
import it.isac.commons.interfaces.ISensorSnapshot;
import it.isac.commons.model.sensors.BaseSensorSnapshot;
import it.isac.commons.model.sensors.SensorType;

public class SensorSourceUser implements ISensor {

	String id;
	boolean active = false;
	
	public SensorSourceUser(String sensorId) {
		this.id = sensorId;
	}
	
	public void setOn() {
		active = true;
	}
	public void setOff() {
		active = false;
	}

	@Override
	public ISensorSnapshot getValue() {
		return new BaseSensorSnapshot(getId(), getType(), Boolean.toString(active));
	}

	@Override
	public String getId() {
		return id;
	}

	@Override
	public String getType() {
		return SensorType.SOURCE;
	}
}
